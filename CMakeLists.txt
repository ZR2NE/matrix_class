cmake_minimum_required (VERSION 3.8 FATAL_ERROR)

set (MATRIX_TESTS        ON CACHE BOOL "Build tests")
set (MATRIX_CREATE_LIB   ON CACHE BOOL "Build lib")
set (PROJECT_NAME_MAT    matrix_class)
set (MATRIX_WORK_VERSION 0.9.0)
set (CMAKE_CXX_STANDARD  11)
set (CMAKE_BINARY_DIR    "${CMAKE_SOURCE_DIR}/build")
set (CMAKE_CACHEFILE_DIR "${CMAKE_SOURCE_DIR}/build")
set (EXECUTABLE_OUTPUT_PATH "${CMAKE_SOURCE_DIR}/bin")
set (LIBRARY_OUTPUT_PATH "${CMAKE_SOURCE_DIR}/lib")

message (STATUS "Configuring...")

project (${PROJECT_NAME_MAT} VERSION "${MATRIX_WORK_VERSION}" LANGUAGES CXX)

include_directories ("${PROJECT_SOURCE_DIR}/inc")

set (MATRIX_LIB_NAME MAT.${MATRIX_WORK_VERSION})
set (MATRIX_EXEC_SOURCES src/main.cpp)

message (STATUS "Build folder is:\n${PROJECT_SOURCE_DIR}")

if (MATRIX_TESTS)
    message (STATUS "Enable tests")
    add_definitions(-DMATRIX_TESTS)
    list (APPEND MATRIX_EXEC_SOURCES src/matrix_test.cpp)
else()
    message (AUTHOR_WARNING "Disable tests")
endif()

add_executable(${PROJECT_NAME_MAT}_exec ${MATRIX_EXEC_SOURCES})

if (MATRIX_CREATE_LIB)
    message (STATUS "Enable to create library")
    add_definitions(-DMATRIX_CREATE_LIB)
    set (MATRIX_LIB_SOURCES  src/matrix.cpp)
    add_library(${MATRIX_LIB_NAME} SHARED ${MATRIX_LIB_SOURCES})
    target_link_libraries(${PROJECT_NAME_MAT}_exec ${MATRIX_LIB_NAME})
else()
    message (AUTHOR_WARNING "Disable create library")
endif()

message (STATUS "Destination:\n${EXECUTABLE_OUTPUT_PATH}/${PROJECT_NAME_MAT}_exec")
