#include "matrix.hpp"
#include <algorithm>
#include <chrono>
#include <iostream>

void Mat_test::Mat_by_size()
{
    Mat<int> matrixS1 (30000, 30000);
    Mat<int> matrixS2 (20000, 30000);
    Mat<int> matrixS3 (30000, 20000);
    if (matrixS1.size().first != 30000 || matrixS1.size().second != 30000 ||
        matrixS2.size().first != 20000 || matrixS2.size().second != 30000 ||
        matrixS3.size().first != 30000 || matrixS3.size().second != 20000) {
        throw std::invalid_argument ("wrong matrix size!");
    }
}

void Mat_test::Mat_by_vector()
{
    std::vector<double> vec = {1., 2., 3., 4., 5., 6.};
    Mat<double> new_matrix (vec);
    for (int i = 0; i < vec.size(); i++) {
        if (new_matrix[i][0] != vec[i]) {
            throw std::invalid_argument ("expected that matrix and vector are equal!");
        }
    }
}

void Mat_test::Mat_by_copy() {
    Mat<double> New_matrix1 (3000, 3000);
    New_matrix1.uniform_();
    Mat<double> New_matrix2 (New_matrix1);

    for (int m = 0; m < New_matrix1.size().first; m++) {
        for (int n = 0; n < New_matrix1.size().second; n++) {
            if (New_matrix1.get(m, n) != New_matrix2.get(m, n)) {
                throw std::invalid_argument ("expected that the matrices are equal!");
            }
        }
    }
}

void Mat_test::getFrobeniusNorm() {
    std::vector<double> vector_Fbn = {1., 2., 2.};
    Mat<double> matrixFbn (vector_Fbn);
    double result = matrixFbn.getFrobeniusNorm();
    if (!(result == 3)) {
        throw std::invalid_argument ("expected that Frobenius norm are equal 3!");
    }
}

void Mat_test::Mat_equality() {
    std::vector<double> vector_eql = {1., 1., 1.};
    Mat<double> matrixLhs (vector_eql);
    Mat<double> matrixRhs (vector_eql);
    if (!(matrixLhs == matrixRhs)) {
        throw std::invalid_argument ("expected that the matrices are equal!");
    }
}

void Mat_test::Mat_not_equality() {
    std::vector<double> vector_lhs = {1., 2., 3.};
    std::vector<double> vector_rhs = {1., 2., 4.};
    Mat<double> matrixLhs (vector_lhs);
    Mat<double> matrixRhs (vector_rhs);
    if (!(matrixLhs != matrixRhs)) {
        throw std::invalid_argument ("expected that the matrices are not equal!");
    }
}

void Mat_test::Mat_addition() {
    std::vector<double> vector_lhs = {1., 2., 3.};
    std::vector<double> vector_rhs = {3., 2., 1.};
    std::vector<double> vector_rst = {4., 4., 4.};
    Mat<double> matrixLhs (vector_lhs);
    Mat<double> matrixRhs (vector_rhs);
    Mat<double> matrixRst (vector_rst);
    Mat<double> matrix_addition = matrixLhs + matrixRhs;
    if (matrix_addition != matrixRst) {
        throw std::invalid_argument ("actual addition result doesn't match expected one!");
    }
}

void Mat_test::Mat_subtraction() {
    std::vector<double> vector_lhs = {4., 3., 2.};
    std::vector<double> vector_rhs = {3., 2., 1.};
    std::vector<double> vector_rst = {1., 1., 1.};
    Mat<double> matrixLhs (vector_lhs);
    Mat<double> matrixRhs (vector_rhs);
    Mat<double> matrixRst (vector_rst);
    Mat<double> matrix_subtraction = matrixLhs - matrixRhs;
    if (matrix_subtraction != matrixRst) {
        throw std::invalid_argument ("actual subtraction result doesn't match expected one!");
    }
}

void Mat_test::Mat_multiplication() {
    std::vector<double> vector_lhs = {1.,  2.,  3.,  4., 5., 7.};
    std::vector<double> vector_rhs = {7.,  5.,  4.,  3., 2., 1.};
    std::vector<double> vector_sqr = {21., 14., 62., 42.};
    std::vector<double> vector_rtg = {13., 9., 6., 33., 23., 16., 56., 39., 27};
    Mat<double> matrixLhs_sqr (2, 3, vector_lhs);
    Mat<double> matrixRhs_sqr (3, 2, vector_rhs);
    Mat<double> matrixLhs_rtg (3, 2, vector_lhs);
    Mat<double> matrixRhs_rtg (2, 3, vector_rhs);
    Mat<double> matrixRst_sqr (2, 2, vector_sqr);
    Mat<double> matrixRst_rtg (3, 3, vector_rtg);
    Mat<double> matrixMltp_sqr = matrixLhs_sqr * matrixRhs_sqr;
    Mat<double> matrixMltp_rtg = matrixLhs_rtg * matrixRhs_rtg;
    if (matrixMltp_sqr != matrixRst_sqr || matrixMltp_rtg != matrixRst_rtg) {
        throw std::invalid_argument ("actual multiplication result doesn't match expected one!");
    }
}

void Mat_test::Mat_solve() {
    Mat<double> A(500, 500), b(500, 1);
    A.uniform_();
    b.uniform_();
    Mat<double> x = solve(A, b);
    double r = (A*x - b).getFrobeniusNorm();
    if (r > 0.001) {
        throw std::invalid_argument ("matrix equation not correctly solved!");
    }
}

void Mat_test_runner::initTestCases()
{
    testCases.push_back (std::pair <void (*)(), std::string> (&Mat_test::Mat_by_size,        std::string ("Mat_by_size       ")));
    testCases.push_back (std::pair <void (*)(), std::string> (&Mat_test::Mat_by_vector,      std::string ("Mat_by_vector     ")));
    testCases.push_back (std::pair <void (*)(), std::string> (&Mat_test::Mat_by_copy,        std::string ("Mat_by_copy       ")));
    testCases.push_back (std::pair <void (*)(), std::string> (&Mat_test::getFrobeniusNorm,   std::string ("getFrobeniusNorm  ")));
    testCases.push_back (std::pair <void (*)(), std::string> (&Mat_test::Mat_equality,       std::string ("Mat_equality      ")));
    testCases.push_back (std::pair <void (*)(), std::string> (&Mat_test::Mat_not_equality,   std::string ("Mat_not_equality  ")));
    testCases.push_back (std::pair <void (*)(), std::string> (&Mat_test::Mat_addition,       std::string ("Mat_addition      ")));
    testCases.push_back (std::pair <void (*)(), std::string> (&Mat_test::Mat_subtraction,    std::string ("Mat_subtraction   ")));
    testCases.push_back (std::pair <void (*)(), std::string> (&Mat_test::Mat_multiplication, std::string ("Mat_multiplication")));
    testCases.push_back (std::pair <void (*)(), std::string> (&Mat_test::Mat_solve,          std::string ("Mat_solve         ")));
}

void Mat_test_runner::runTests()
{
    initTestCases();

    auto testExecutor = [](std::pair <void (*)(), std::string> testInfo) {
        std::chrono::time_point<std::chrono::system_clock> start, end;
        start = std::chrono::system_clock::now();
        try {
            testInfo.first();
        }
        catch (const std::exception& e)
        {
            std::cout << "TEST: " << testInfo.second << "\tFAILED" << std::endl;
            std::cout << "Fail message: " << std::string(e.what()) << std::endl;
            return;
        }
        std::cout << "TEST: " << testInfo.second << "\tPASSED, ";
        end = std::chrono::system_clock::now();
        int elapsed_seconds = std::chrono::duration_cast<std::chrono::microseconds> (end - start).count();
        std::cout << "spent time: " << elapsed_seconds << " microseconds"<< std::endl;
        };
    std::for_each (testCases.begin(), testCases.end(), testExecutor);
}
