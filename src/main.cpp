#include <iostream>
#include "matrix.hpp"

using namespace std;

int main()
{
    #if MATRIX_TESTS
        Mat_test_runner runner; runner.runTests();
    #endif
    #if MATRIX_CREATE_LIB
        Dummy::foo();
    #endif
    return 0;
}
