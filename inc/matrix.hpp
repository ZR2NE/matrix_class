#include <cstddef>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <cmath>

#if MATRIX_TESTS
    #include "matrix_test.hpp"
#endif

#if MATRIX_CREATE_LIB
    class Dummy
    {
    public:
        static void foo();
    };
#endif

template <typename T>
class Mat;

template <typename T>
Mat<T> solve(const Mat<T>& A, const Mat<T>& b) {
    static_assert(std::is_floating_point<T>::value, "matrix must consist only floating point values to implement solve() function");
    if (A.size().first != b.size().first) {
        throw std::invalid_argument ("b matrix-vector must have as many rows as matrix A");
    }
    else if (b.size().second != 1) {
        throw std::invalid_argument ("matrix b is not a vector");
    }
    else if ((A.size().first - A.size().second) != 0) {
        throw std::invalid_argument ("matrix A must be square");
    }
    Mat<T> tmpMat(A.size().first, A.size().second + 1);
    T* tmpMatArray = tmpMat.data(0);
    for (int i = 0; i != tmpMat.size().first; ++i) {
        for (int j = 0; j != A.size().second; ++j) {
            tmpMat[i][j] = A.get(i, j);
        }
        tmpMat[i+1][-1] = b.get(i, 0);
    }
    T current = 0;
    for (int i=0; i<tmpMat.size().first; ++i) {
        current = tmpMat.get (i, i);
        for (int j=tmpMat.size().first; j>=i; --j) {
            tmpMat[i][j] = tmpMat.get (i, j) / current;
        }
        for (int j=i+1; j<tmpMat.size().first; ++j) {
            current = tmpMat.get (j, i);
            for (int k=tmpMat.size().first; k>=i; --k) {
                tmpMat[j][k] -= current * tmpMat.get (i, k);
            }
        }
    }
    std::vector <T> x;
    x.resize (A.size().second);
    for (int i=tmpMat.size().first -1; i>=0; i--) {
        x[i] = tmpMat.get(i, tmpMat.size().second - 1);
        for (int k=0; k<i; ++k) {
            tmpMatArray[(k+1) * tmpMat.size().second -1] -= tmpMat.get(k, i) * x[i];
        }
    }
    Mat<T> resultMat(x);
    return resultMat;
}

template <typename T>
class Mat
{
    static_assert (std::is_arithmetic <T>::value, "Only for arithmetic types");

public:

    Mat (std::size_t m, std::size_t n);
    Mat (std::size_t m, std::size_t n, const std::vector<T> &vec);
    Mat (const std::vector<T> &vec);
    Mat (const T* mem, int m);
    Mat (const Mat& other);
    Mat();
    ~Mat();

    const T& get (std::size_t i, std::size_t j) const;
    T* data (std::size_t i) const;
    void uniform_();
    T getFrobeniusNorm () const;
    void swapColumn (std::size_t i, std::size_t j);
    std::pair <size_t, size_t> size() const {return std::pair <size_t, size_t> (myM, myN);}

    template <typename R> friend std::ostream& operator<<   (std::ostream &out, const Mat<R> &myMatrix);
    template <typename R> friend bool          operator==   (const Mat<R>& lhs, const Mat<R>& rhs);
    template <typename R> friend bool          operator!=   (const Mat<R>& lhs, const Mat<R>& rhs);
    template <typename R> friend Mat<R>        operator+    (const Mat<R>& lhs, const Mat<R>& rhs);
    template <typename R> friend Mat<R>        operator-    (const Mat<R>& lhs, const Mat<R>& rhs);
    template <typename R> friend Mat<R>        operator*    (const Mat<R>& lhs, const Mat<R>& rhs);
    template <typename R> friend Mat<R>        operator*    (const Mat<R>& lhs, const T alpha);
    T*                                         operator[]   (const std::size_t i);

private:

    T* myMatrix;
    std::size_t myM; //Число строк
    std::size_t myN; //Число столцов
};


template <typename T>
bool operator== (const Mat<T>& lhs, const Mat<T>& rhs)
{
    std::size_t mlhs = lhs.size().first;
    std::size_t nlhs = lhs.size().second;
    std::size_t mrhs = rhs.size().first;
    std::size_t nrhs = rhs.size().second;
    if (mlhs != mrhs || nlhs != nrhs) {
        return false;
    }
    std::size_t size = mlhs * nlhs;
    for (int i = 0; i < size; i++) {
        if (lhs.myMatrix[i] != rhs.myMatrix[i]) {
            return false;
        }
    }
   return true;
}

template <typename T>
bool operator!= (const Mat<T>& lhs, const Mat<T>& rhs)
{
   return !(lhs == rhs);
}

template <typename T>
Mat<T> operator+ (const Mat<T>& lhs, const Mat<T>& rhs)
{
    std::size_t m = lhs.size().first;
    std::size_t n = lhs.size().second;
    if (m != rhs.size().first || n != rhs.size().second) {
        throw std::invalid_argument
                ("Exception occurred in operator \"+\" with message: \"out of range!\"");
    }
    std::size_t size = m * n;
    Mat<T> resultMat (m, n);
    T* resultArray = resultMat.data(0);
    for (int i = 0; i < size; i++) {
        resultArray[i] = lhs.myMatrix[i] + rhs.myMatrix[i];
    }
    return resultMat;
}

template <typename T>
Mat<T> operator- (const Mat<T>& lhs, const Mat<T>& rhs)
{
    std::size_t m = lhs.size().first;
    std::size_t n = lhs.size().second;
    std::size_t size = m * n;
    Mat<T> resultMat (m, n);
    T* resultArray = resultMat.data(0);
    for (int i = 0; i < size; i++) {
        resultArray[i] = lhs.myMatrix[i] - rhs.myMatrix[i];
    }
    return resultMat;
}

template <typename T>
Mat<T> operator* (const Mat<T>& lhs, const T alpha)
{
    std::size_t m = lhs.size().first;
    std::size_t n = lhs.size().second;
    std::size_t size = m * n;
    Mat<T> resultMat (m, n);
    T* resultArray = resultMat.data(0);
    for (int i = 0; i < size; i++) {
        resultArray[i] = lhs.myMatrix[i] * alpha;
    }
    return resultMat;
}

template <typename T>
Mat<T> operator* (const Mat<T>& lhs, const Mat<T>& rhs)
{
    std::size_t m_lhs = lhs.size().first;
    std::size_t n_lhs = lhs.size().second;
    std::size_t m_rhs = rhs.size().first;
    std::size_t n_rhs = rhs.size().second;
    std::size_t r_size = m_lhs * n_rhs;
    if (n_lhs != m_rhs) {
        throw std::invalid_argument ("Such a matrix size can not be multiplied!");
    }
    Mat<T> resultMat (m_lhs, n_rhs);
    T* resultArray = resultMat.data(0);
    for (int i = 0; i < r_size; i++) {
      resultArray[i] = 0;
    }
    for (int i = 0; i < m_lhs; i++) {
        for (int j = 0; j < n_rhs; j++) {
            for (int k = 0; k < n_lhs; k++) {
                resultArray[i * n_rhs + j] += lhs.get(i, k) * rhs.get (k, j);
            }
        }
    }
    return resultMat;
}

template <typename T>
Mat<T>::Mat (std::size_t m, std::size_t n)
{
    myM = m;
    myN = n;
    std::size_t mySize = m * n;
    myMatrix = new T [mySize];
}

template <typename T>
Mat<T>::Mat (std::size_t m, std::size_t n, const std::vector<T> &vec)
{
    myM = m;
    myN = n;
    std::size_t mySize = myM * myN;
    myMatrix = new T [mySize];
    for (std::size_t i = 0; i < mySize; i++) {
        myMatrix[i] = vec[i];
    }
}

template <typename T>
Mat<T>::Mat (const std::vector<T> &vec)
{
    myM = vec.size();
    myN = 1;
    myMatrix = new T [myM];
    for (int i = 0; i < myM; i++) {
        myMatrix[i] = vec[i];
    }
}

template <typename T>
Mat<T>::Mat (const Mat& other) {
    myM = other.size().first;
    myN = other.size().second;
    std::size_t mySize = myM * myN;
    myMatrix = new T [mySize];
    for (int i = 0; i < myM; i++) {
        for (int j = 0; j < myN; j++) {
            myMatrix [i * myN + j] = other.get(i, j);
        }
    }
}

template <typename T>
Mat<T>::~Mat()
{
   std::size_t mySize = myM * myN;
   delete[] myMatrix;
}

template <typename T>
const T& Mat<T>::get (std::size_t i, std::size_t j) const
{
    if (i >= myM || j >= myN) {
        throw std::invalid_argument ("Out of range!");
    }
    return myMatrix [i * myN + j];
}

template <typename T>
T* Mat<T>::data (std::size_t i) const
{
    if (i > myM) {
        throw std::invalid_argument ("Out of range!");
    }
    return myMatrix + i * myN;
}

template <typename T>
void Mat<T>::uniform_()
{
    std::size_t mySize = myM * myN;
    for (std::size_t i = 0; i < mySize; i++) {
        myMatrix [i] = 0.1 * (rand() % 101);
    }
}

template <typename T>
T Mat<T>::getFrobeniusNorm () const
{
    double result = 0;
    for (int i = 0; i < myM; i++) {
        for (int j = 0; j < myN; j++) {
            result += pow (myMatrix [i * myN + j], 2);
        }
    }
    return sqrt (result);
}

template <typename T>
void Mat<T>::swapColumn (std::size_t i, std::size_t j)
{
    for (int k = 0; k < myN; k++) {
        T tmp = myMatrix [k * myN + i];
        myMatrix [k * myN + i] = myMatrix [k * myN + j];
        myMatrix [k * myN + j] = tmp;
    }
}

template <typename T>
std::ostream& operator<< (std::ostream &out, const Mat<T> &myMat)
{
    for (int m = 0; m < myMat.myM; m++) {
        for (int n = 0; n < myMat.myN; n++) {
            out << myMat.myMatrix [m * myMat.myN + n];
            if (n < myMat.myN - 1) {
                out << "\t";
            }
        }
        out << "\n";
    }
    return out;
}

template <typename T>
T* Mat<T>::operator[] (std::size_t i)
{
    return data (i);
}
