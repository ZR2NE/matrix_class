class Mat_test
{
public:
    static void Mat_by_size();
    static void Mat_by_vector();
    static void Mat_by_array();
    static void Mat_by_copy();
    static void getFrobeniusNorm();
    static void Mat_equality();
    static void Mat_not_equality();
    static void Mat_addition();
    static void Mat_subtraction();
    static void Mat_multiplication();
    static void Mat_solve();
};

class Mat_test_runner
{
public:
    void runTests();

private:
    void initTestCases ();
    std::vector <std::pair <void (*)(), std::string>> testCases;
};
